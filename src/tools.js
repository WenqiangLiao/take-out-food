function parseInputToMap(selectedItems) {
  // need to be implement
  let rowItems = selectedItems.split(",");
  let map = [];
  rowItems.forEach(element => {
    let itemSplit = [element.split(" x ")[0], parseInt(element.split(" x ")[1])];
    map.push(itemSplit);
  });
  return new Map(map);
}

export { parseInputToMap };
