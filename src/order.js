import dishes from './menu.js';

class Order {
  constructor(itemsMap) {
    // Need to be implement
    this.dishes = dishes;
    this.itemsMap = itemsMap;
  }

  get itemDetails() {
    let order = [];
    for(let item of this.itemsMap){
      this.dishes.forEach(element => {
        if(element.id === item[0]){
          element.count = item[1];
          order.push(element);
        }
      })
    }
    return order;
  }

  get totalPrice() {
    // Need to be implement
    let totalMoney = 0;
    for(let item of this.itemsMap){
      this.dishes.forEach(element => {
        if(item[0] === element.id){
          totalMoney += element.price*item[1];
        }
      });
    }
    return totalMoney;
  }
}

export default Order;
