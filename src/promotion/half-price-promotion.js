import Promotion from './promotion.js';

class HalfPricePromotion extends Promotion {
  constructor(order) {
    super(order);
    this._type = '指定菜品半价';
    this.halfPriceDishes = ['ITEM0001', 'ITEM0022'];
  }

  get type() {
    return this._type;
  }

  includedHalfPriceDishes() {
    // need to be completed 获得所含半价菜品信息
    let orderDetails = this.order.itemDetails;
    let halfPriceDishes = [];
    orderDetails.forEach(element => {
      if(this.halfPriceDishes.includes(element.id)){
        halfPriceDishes.push(element);
      }
    });
    return halfPriceDishes;
  }

  discount() {
    // need to be completed
    let orderDetails = this.order.itemDetails;
    let discountMoney = 0;
    orderDetails.forEach(element => {
      if(this.halfPriceDishes.includes(element.id)){
        discountMoney = discountMoney + element.count*element.price*0.5;
      }
    });
    return discountMoney;
  }
}

export default HalfPricePromotion;
