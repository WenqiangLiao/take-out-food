import Promotion from './promotion.js';

class OverMinusPromotion extends Promotion {
  constructor(order) {
    super(order);
    this._type = '满30减6元';
  }

  get type() {
    return this._type;
  }

  discount() {
    // Need to be implemented
    let discountMoney = 0; 
    if(this.order.totalPrice >= 30){
      discountMoney = 6;
    }
    return discountMoney;
  }
}

export default OverMinusPromotion;
